import { nanoid } from "nanoid";
import useAlbums from "./Hooks/useAlbums";
import { useStore } from "./Stores";

const MusicList = () => {
  const { music } = useStore();
  const loading = useAlbums();
  return loading ? (
    <h5>Loading</h5>
  ) : (
    <section>
      <ul>
        {music.albums.map((album) => (
          <li key={nanoid()}>{album.title}</li>
        ))}
      </ul>
    </section>
  );
};

export default MusicList;
