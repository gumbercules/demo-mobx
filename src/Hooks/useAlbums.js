import { useState, useEffect } from "react";
import { useStore } from "../Stores";

const useAlbums = () => {
  const [loading, setLoading] = useState(true);
  const { music } = useStore();
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/albums")
      .then(async (res) => {
        const albums = await res.json();
        setTimeout(() => {
          music.addAlbums(albums);
          setLoading(false);
        }, 1500);
      })
      .catch((err) => {
        setLoading(false);
        console.error(err);
      });
  });
  return loading;
};

export default useAlbums;
