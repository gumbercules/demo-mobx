import { observer } from "mobx-react";

import { nanoid } from "nanoid";
import BookListItem from "./BookListItem";
import { useStore } from "./Stores";

const BookList = observer(() => {
  const { main } = useStore();

  function addBook() {
    main.addBook({ title: "This is a second book." });
  }

  return (
    <section>
      <h3>{main.userName}</h3>
      <ul>
        {main.books.map((book) => (
          <BookListItem key={nanoid()} title={book.title} />
        ))}
      </ul>
      <button onClick={addBook}>Add Book</button>
    </section>
  );
});

export default BookList;
