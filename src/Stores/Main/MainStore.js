import { makeAutoObservable } from "mobx";

class MainStore {
  user = {
    firstName: "Eric",
    email: "awesome@123.com",
  };

  books = [
    {
      title: "This is a demo book",
    },
  ];

  get userName() {
    return this.user.firstName;
  }

  constructor() {
    makeAutoObservable(this);
  }

  addBook(newBook) {
    this.books.push(newBook);
  }
}

export default new MainStore();
