import { createContext, useContext } from "react";
import MainStore from "./MainStore";

const MainContext = createContext();

export const MainProvider = ({ children }) => {
  return (
    <MainContext.Provider value={MainStore}>{children}</MainContext.Provider>
  );
};

export const useMainStore = () => {
  return useContext(MainContext);
};

export default MainContext;
