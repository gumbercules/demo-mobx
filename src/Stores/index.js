import { createContext, useContext } from "react";
import MainStore from "./Main/MainStore";
import MusicStore from "./Music/MusicStore";

const StoreContext = createContext();

export const StoreProvider = ({ children }) => {
  return (
    <StoreContext.Provider
      value={{
        main: MainStore,
        music: MusicStore,
      }}
    >
      {children}
    </StoreContext.Provider>
  );
};

export const useStore = () => {
  return useContext(StoreContext);
};
