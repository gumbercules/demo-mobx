import { makeAutoObservable } from "mobx";

class MusicStore {
  albums = [];

  constructor() {
    makeAutoObservable(this);
  }

  addAlbums(albumList) {
    this.albums = [...this.albums, ...albumList];
  }
}

export default new MusicStore();
