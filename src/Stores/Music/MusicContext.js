import { useContext, createContext } from "react";
import MusicStore from "./MusicStore";

const MusicContext = createContext();

export const MusicProvider = ({ children }) => {
  return (
    <MusicContext.Provider value={MusicStore}>{children}</MusicContext.Provider>
  );
};

export const useMusicStore = () => {
  return useContext(MusicContext);
};
