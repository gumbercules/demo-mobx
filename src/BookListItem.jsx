const BookListItem = (props) => {
  const { title } = props;

  return <li>{title}</li>;
};

export default BookListItem;
