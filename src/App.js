import BookList from "./BookList";

import "./App.css";

import { StoreProvider } from "./Stores";
import MusicList from "./MusicList";

function App() {
  return (
    <StoreProvider>
      <div className="App">
        <BookList />
        <MusicList />
      </div>
    </StoreProvider>
  );
}

export default App;
